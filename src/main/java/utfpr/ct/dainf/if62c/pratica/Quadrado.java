/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author LucianoTadeu
 */
public class Quadrado extends Retangulo {

    public Quadrado() {
        super();
    }

    public Quadrado(double lado) {
        super(lado, lado);
    }

    public double getLado() {
        return getBase();
    }

    @Override
    public String toString() {
        return getNome() + " [" + getLado() + "]";
    }

}
