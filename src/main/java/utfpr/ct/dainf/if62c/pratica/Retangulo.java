/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author LucianoTadeu
 */
public class Retangulo implements FiguraComLados {
    private double base, altura;
    
    public Retangulo() {
    }

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public double getLadoMaior() {
        return this.getBase();
    }
    
    @Override
    public double getLadoMenor() {
        return this.getAltura();
    }
    
    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }
    
    @Override
    public String toString() {
        return super.toString() + " [" + base + " x " + altura + "]";
    }
    
    @Override
    public double getArea() {
        return getBase() * getAltura();
    }

    @Override
    public double getPerimetro() {
        return (getBase() + getAltura()) * 2;
    }
}

