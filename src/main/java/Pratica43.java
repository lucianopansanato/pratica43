
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LucianoTadeu
 */
public class Pratica43 {
    public static void main(String[] args) {
        Elipse e = new Elipse(20, 15);
        Circulo c = new Circulo(15);
        Retangulo r = new Retangulo(20, 15);
        Quadrado q = new Quadrado(20);
        TrianguloEquilatero t = new TrianguloEquilatero(20);
        
        System.out.println("Area Elipse = " + e.getArea());
        System.out.println("Perímetro Elipse = " + e.getPerimetro());
        System.out.println("Area Circulo = " + c.getArea());
        System.out.println("Perímetro Circulo = " + c.getPerimetro());
        System.out.println("Area Retangulo = " + r.getArea());
        System.out.println("Perímetro Retangulo = " + r.getPerimetro());
        System.out.println("Area Quadrado = " + q.getArea());
        System.out.println("Perímetro Quadrado = " + q.getPerimetro());
        System.out.println("Area Triangulo Equilatero = " + t.getArea());
        System.out.println("Perímetro Triangulo Equilatero = " + t.getPerimetro());        
    }         
}
